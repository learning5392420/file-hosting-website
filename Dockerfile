FROM ubuntu:20.04

RUN apt update && apt install -y python3 && \ 
apt install -y python3-venv && \
apt install -y python3-pip   

ARG WEB_APP_NAME

COPY . /srv/$WEB_APP_NAME
WORKDIR /srv/$WEB_APP_NAME
RUN python3 -m venv ./filesharing_venv && . filesharing_venv/bin/activate && pip3 install -r requirements.txt

ENV WEB_APP_NAME=$WEB_APP_NAME
ENV PATH="/srv/$WEB_APP_NAME/filesharing_venv/bin:$PATH"
ENV SECRET_ENV="sanya"

EXPOSE 8000

CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]
 
